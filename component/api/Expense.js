import React, { useState, useEffect, useCallback } from 'react';
import { StyleSheet, Text, View} from 'react-native';
import { DataTable } from 'react-native-paper';
import RNPickerSelect from 'react-native-picker-select';



export function Expense() {
    const [expense, setList] = useState([]);

    useEffect(() => {

        fetch('http://192.168.1.155/apigestion/public/expenses')
            .then(data => data.json())
            .then(items => {
                setList(items)
                console.log(expense)
            })

    }, [])

    return expense;
}

export function Display(){

    const [select, setSelect] = useState("all");
    const [selectYear, setSelectYear] = useState("all");
    const [selectMonth, setSelectMonth] = useState("all");
    const listExpense = Expense();
    const totu = ({
        value: "all",
        label: "all"
    })



        return(
            <View>
                <RNPickerSelect
                    onValueChange={value => setSelect(value)}
                    items={Category()}
                    placeholder={totu}
                />
                <RNPickerSelect
                    onValueChange={value => setSelectYear(value)}
                    items={[
                        { label: '2015', value: '2015' },
                        { label: '2016', value: '2016' },
                        { label: '2017', value: '2017' },
                        { label: '2018', value: '2018' },
                        { label: '2019', value: '2019' },
                        { label: '2020', value: '2020' },
                        { label: '2021', value: '2021' },
                        { label: '2022', value: '2022' },
                    ]}
                    placeholder={totu}
                />
                <RNPickerSelect
                    onValueChange={value => setSelectMonth(value)}
                    items={[
                        { label: 'Janvier (01)', value: '01' },
                        { label: 'Février (02)', value: '02' },
                        { label: 'Mars (03)', value: '03' },
                        { label: 'Avril (04)', value: '04' },
                        { label: 'Mai (05)', value: '05' },
                        { label: 'Juin (06)', value: '06' },
                        { label: 'Juillet (07)', value: '07' },
                        { label: 'Août (08)', value: '08' },
                        { label: 'Septembre (09)', value: '09' },
                        { label: 'Octobre (10)', value: '10' },
                        { label: 'Novembre (11)', value: '11' },
                        { label: 'Décembre (12)', value: '12' },
                    ]}
                    placeholder={totu}
                />
                <DataTable>
                    <DataTable.Header>
                        <DataTable.Title>Name</DataTable.Title>
                        <DataTable.Title>Amount</DataTable.Title>
                        <DataTable.Title>Category</DataTable.Title>
                        <DataTable.Title>Date</DataTable.Title>
                    </DataTable.Header>
                    {listExpense.filter(cat => cat.category.id == select || select === "all").filter(annee => annee.date.split('-')[0] == selectYear || selectYear === "all").filter(mois => mois.date.split('-')[1] == selectMonth || selectMonth === "all").map(item =>
                        <DataTable.Row key={item.id}>
                            <DataTable.Cell>{item.name}</DataTable.Cell>
                            <DataTable.Cell>{item.amout}</DataTable.Cell>
                            <DataTable.Cell>{item.category.name}</DataTable.Cell>
                            <DataTable.Cell>{item.date}</DataTable.Cell>
                        </DataTable.Row>
                    )}
                </DataTable>
            </View>
        )

}

export function Category(){
    const [category, setList] = useState([]);

    useEffect(() => {

        fetch('http://192.168.1.155/apigestion/public/categories')
            .then(data => data.json())
            .then(items => {
                setList(items)
                console.log(category)
            })

    }, [])

    const options = category.map(categ => ({
            "value" : categ.id,
            "label" : categ.name
    })
    )

    return options;

}